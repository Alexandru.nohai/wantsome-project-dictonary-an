import Utils.Utils;
import fileReader.ResourceInputFileReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

public class UtilsTests {
    @Test
    public void testPalindromeFinder() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("unu");
        Set<String> palindromes = Utils.findAllPalindromes(inputSet);

        Assert.assertTrue(palindromes.contains("unu"));
        Assert.assertEquals(1, palindromes.size());
    }

    @Test
    public void testPalindromesFromFile() throws IOException {
        List<String> words = new ArrayList<>();
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader();
        words = resourceInputFileReader.readFile("dex.txt");
        Set<String> wordsNoDuplicate = Utils.removeDuplicate(words);
        Set<String> palindromes = Utils.findAllPalindromes(wordsNoDuplicate);
        Assert.assertTrue(palindromes.contains("aba"));
    }

    @Test
    public void testSorting() {
        String word = "Actor";
        System.out.println(Utils.sortCharachters(word));
    }

    @Test
    public void testAnagramsFinder(){
        Set<String> list = new HashSet<>();
        list.add("abile");
        list.add("labei");
        list.add("baile");
        list.add("biela");
        list.add("albie");
        list.add("faraLegatura");

        Map<String, List<String>> anagrame = Utils.findAnagrams(list);
        Assert.assertTrue(anagrame.containsKey("abeil"));
        Assert.assertEquals(2, anagrame.size());

    }
}
