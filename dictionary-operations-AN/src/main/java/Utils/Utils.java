package Utils;

import java.util.*;

public class Utils {
    public static Set<String> removeDuplicate(List<String> allLines) {
        Set<String> wordsSet = new HashSet<>();

        for (String line : allLines) {
            wordsSet.add(line);
        }
        return wordsSet;
    }

    public static Set<String> findWordsThatContainsASpecificString(Set<String> wordsSet, String word) {
        Set<String> result = new HashSet<>();
        for (String line : wordsSet) {
            if (line.contains(word)) {
                result.add(line);
            }
        }
        return result;
    }

    public static Set<String> findAllPalindromes(Set<String> wordsSet) {
        Set<String> result = new HashSet<>();

        for (String line : wordsSet) {
            StringBuilder reverseWord = new StringBuilder(line).reverse();
            if (line.equals(reverseWord.toString())) {
                result.add(line);
            }
        }
        return result;
    }

    public static String sortCharachters(String word) {
        char[] a = word.toLowerCase().toCharArray();
        Arrays.sort(a);
        String sortedLetters = new String(a);
        return sortedLetters;
    }

    public static Map<String, List<String>> findAnagrams(Set<String> wordsSet) {
        Map<String, List<String>> anagrams = new HashMap<>();
        String[] wordsArray = wordsSet.toArray(new String[0]);

        for (String i : wordsArray) {
            String word = String.valueOf(i);
            String sortedCharactersFromWord = sortCharachters(word);

            if (anagrams.containsKey(sortedCharactersFromWord)) {
                anagrams.get(sortedCharactersFromWord).add(word);
            } else {
                List<String> words = new ArrayList<>();
                words.add(word);
                anagrams.put(sortedCharactersFromWord, words);
            }
        }
        return anagrams;
    }
}
