package dictionaryOperations;

import Utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

public class AnagramDictionaryOperation implements DictionaryOperation{
    private Set<String> wordsSet;
    private BufferedReader in;
    public AnagramDictionaryOperation (Set<String> wordsSet, BufferedReader in){
        this.wordsSet = wordsSet;
        this.in = in;
    }

    @Override
    public void run() throws IOException {
        Map<String, List<String>> anagrams = Utils.findAnagrams(wordsSet);
        System.out.println("Please type a word: ");
        String userInput = in.readLine();
        String sortedInput = Utils.sortCharachters(userInput);
        if (anagrams.containsKey(sortedInput)){
            anagrams.get(sortedInput).add(userInput);
        }else {
            System.out.println("No anagram found, please try another word");
        }
        System.out.println(anagrams.get(sortedInput));
    }
}
